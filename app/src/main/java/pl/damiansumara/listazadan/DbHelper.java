package pl.damiansumara.listazadan;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context) {
        super(context, "ToDoList", null, 10);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format("CREATE TABLE task (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "name TEXT NOT NULL, " +
            "priority INTEGER NOT NULL, " +
            "done BOOLEAN NOT NULL, " +
            "creation_date INTEGER NOT NULL, " +
            "due_date INTEGER NOT NULL, " +
                // I'm too lazy to do sort in java so,
                // Quick hax to easily sort via due date (otherwise it would show tasks WITHOUT due date first instead of last<in ASC ORDER>)
            "has_due_date BOOLEAN NOT NULL, " +
            "completion_date INTEGER NOT NULL, " +
            "notification_was_sent BOOLEAN NOT NULL" +
            ");");
        db.execSQL(query);

        query = String.format("CREATE TABLE task_attachment_image (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "task_id FOREIGN_KEY REFERENCES task(id) ON DELETE CASCADE, " +
                "data BLOB NOT NULL" +
                ");");

        db.execSQL(query);

        query = String.format("CREATE TABLE task_attachment_video (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "task_id FOREIGN_KEY REFERENCES task(id) ON DELETE CASCADE, " +
                "uri TEXT NOT NULL" +
                ");");

        db.execSQL(query);
    }

    /* TASKS */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = String.format("DROP TABLE IF EXISTS task");
        db.execSQL(query);

        query = String.format("DROP TABLE IF EXISTS task_attachment_image");
        db.execSQL(query);

        query = String.format("DROP TABLE IF EXISTS task_attachment_video");
        db.execSQL(query);
        onCreate(db);
    }

    public void newTask(ToDoTask task) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("name", task.getName());
        values.put("priority", task.getPriority());
        values.put("done", false);
        values.put("creation_date", System.currentTimeMillis());
        values.put("due_date", task.getDueDate().getTime());
        values.put("has_due_date", task.getDueDate().getTime() > 0);
        values.put("completion_date", 0); // no completion date yet
        values.put("notification_was_sent", false);

        db.insertWithOnConflict("task", null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public void updateTask(int taskId, String taskName, int priority, Date due_date)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values= new ContentValues();

        final boolean dueDateChanged = getTask(taskId).getDueDate().getTime() != due_date.getTime();

        values.put("name", taskName);
        values.put("priority", priority);
        values.put("due_date", due_date.getTime());
        values.put("has_due_date", due_date.getTime() > 0);
        if(dueDateChanged)
        {
            values.put("notification_was_sent", false);
        }

        db.update("task", values, "id=?", new String[] {String.valueOf(taskId)});
        db.close();
    }

    public void setTaskStatus(int taskId, boolean completed)
    {
        final ToDoTask task = getTask(taskId);
        if(task.getDone() == completed)
        {
            return;
        }

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("done", completed);
        values.put("completion_date", completed ? System.currentTimeMillis() : 0);

        db.update("task", values, "id=?", new String[] {String.valueOf(taskId)});
        db.close();
    }

    public void setNotificationStatus(int taskId, boolean sent)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("notification_was_sent", sent);

        db.update("task", values, "id=?", new String[] {String.valueOf(taskId)});
        db.close();
    }

    public void deleteTask(ToDoTask task){
        deleteTask(task.getId());
    }

    public void deleteTask(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete("task", "id=?", new String[] {String.valueOf(id)});
        db.close();
    }

    public void deleteCompletedTasks()
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM task WHERE done = 1");
        db.close();
    }

    public void deleteAllTasks()
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM task");
        db.close();
    }

    public ToDoTask getTaskFromCursor(Cursor cursor)
    {
        int id = cursor.getInt(cursor.getColumnIndex("id"));
        String name = cursor.getString(cursor.getColumnIndex("name"));
        int priority = cursor.getInt(cursor.getColumnIndex("priority"));
        Boolean done = cursor.getInt(cursor.getColumnIndex("done")) > 0;
        long creationDate = cursor.getLong(cursor.getColumnIndex("creation_date"));
        long dueDate = cursor.getLong(cursor.getColumnIndex("due_date"));
        long completionDate = cursor.getLong(cursor.getColumnIndex("completion_date"));

        return new ToDoTask(id, name, priority, done, new Date(creationDate), new Date(dueDate), new Date(completionDate));
    }

    public ToDoTask getTask(int taskId)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("task", null, "id=?", new String[] {String.valueOf(taskId)}, null, null, null);
        while(cursor.moveToFirst())
        {
            return getTaskFromCursor(cursor);
        }

        return null;
    }

    public ArrayList<ToDoTask> getTaskList()
    {
        return getTaskList(null);
    }

    public ArrayList<ToDoTask> getTaskList(SortSettings sortSettings, EFilterType filterType)
    {
        switch (filterType)
        {
            case NONE:
                return getTaskList(sortSettings);

            case NOT_COMPLETED:
                return getTaskListByStatus(sortSettings, false);

            case COMPLETED:
                return getTaskListByStatus(sortSettings, true);

            case OVERDUE:
                return getOverdueTasks(sortSettings);

            case DUE_TODAY:
                return getTasksDueToday(sortSettings);

            case DUE_IN_24H:
                final long dayInMs = (long)24 * 60 * 60 * 1000;
                return getTasksDueIn(sortSettings, dayInMs);

            case DUE_IN_7D:
                final long weekInMs = (long)7 * 24 * 60 * 60 * 1000;
                return getTasksDueIn(sortSettings, weekInMs);

            case DUE_IN_30D:
                long monthInMs = (long)30 * 24 * 60 * 60 * 1000;
                return getTasksDueIn(sortSettings, monthInMs);

            default:
                return getTaskList(sortSettings);
        }
    }

    public ArrayList<ToDoTask> getTaskList(SortSettings sortSettings)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<ToDoTask> tasksList = new ArrayList<>();

        Cursor cursor = db.query("task", null, null, null, null, null, getOrderBy(sortSettings));
        while(cursor.moveToNext())
        {
            tasksList.add(getTaskFromCursor(cursor));
        }

        cursor.close();
        db.close();

        return tasksList;
    }

    public ArrayList<ToDoTask> getTasksDueIn(SortSettings sortSettings, long dueIn)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<ToDoTask> tasksList = new ArrayList<>();

        final long currentTime = System.currentTimeMillis();
        final String selection = "done = ? AND has_due_date = ? AND due_date >= ? AND due_date <= ?";
        final String[] selectionArguments = new String[]{"0", "1", String.valueOf(currentTime), String.valueOf((currentTime + dueIn))};

        Cursor cursor = db.query("task", null, selection, selectionArguments, null, null, getOrderBy(sortSettings));
        while(cursor.moveToNext())
        {
            tasksList.add(getTaskFromCursor(cursor));
        }

        cursor.close();
        db.close();

        return tasksList;
    }

    public ArrayList<ToDoTask> getTasksDueInForNotifcation(SortSettings sortSettings, long dueIn)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<ToDoTask> tasksList = new ArrayList<>();

        final long currentTime = System.currentTimeMillis();
        final String selection = "done = ? AND notification_was_sent = ? AND has_due_date = ? AND due_date >= ? AND due_date <= ?";
        final String[] selectionArguments = new String[]{"0", "0", "1", String.valueOf(currentTime), String.valueOf((currentTime + dueIn))};

        Cursor cursor = db.query("task", null, selection, selectionArguments, null, null, getOrderBy(sortSettings));
        while(cursor.moveToNext())
        {
            tasksList.add(getTaskFromCursor(cursor));
        }

        cursor.close();
        db.close();

        return tasksList;
    }

    public ArrayList<ToDoTask> getTasksDueToday(SortSettings sortSettings)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<ToDoTask> tasksList = new ArrayList<>();

        // Use joda time once upgraded to api26
        Calendar date = new GregorianCalendar();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        final long dayBeginsAt = date.getTimeInMillis();
        date.add(Calendar.DAY_OF_MONTH, 1);
        final long dayEndsAt = date.getTimeInMillis() - 1;

        final String selection = "done = ? AND has_due_date = ? AND due_date >= ? AND due_date <= ?";
        final String[] selectionArguments = new String[]{"0", "1", String.valueOf(dayBeginsAt), String.valueOf(dayEndsAt)};

        Cursor cursor = db.query("task", null, selection, selectionArguments, null, null, getOrderBy(sortSettings));

        Log.v("DUE", selectionArguments[2] + " " + selectionArguments[3]);


        while(cursor.moveToNext())
        {
            tasksList.add(getTaskFromCursor(cursor));
        }

        cursor.close();
        db.close();

        return tasksList;
    }

    public ArrayList<ToDoTask> getTaskListByStatus(SortSettings sortSettings, boolean completed)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<ToDoTask> tasksList = new ArrayList<>();


        Cursor cursor = db.query("task", null, "done = ?", new String[]{completed ? "1" : "0"}, null, null, getOrderBy(sortSettings));


        while(cursor.moveToNext())
        {
            tasksList.add(getTaskFromCursor(cursor));
        }

        cursor.close();
        db.close();

        return tasksList;
    }

    public ArrayList<ToDoTask> getOverdueTasks(SortSettings sortSettings)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<ToDoTask> tasksList = new ArrayList<>();

        final String selection = "done = ? AND has_due_date = ? AND due_date <= ?";
        final String[] selectionArguments = new String[]{"0", "1", String.valueOf(System.currentTimeMillis())};

        Cursor cursor = db.query("task", null, selection, selectionArguments, null, null, getOrderBy(sortSettings));

        while(cursor.moveToNext())
        {
            tasksList.add(getTaskFromCursor(cursor));
        }

        cursor.close();
        db.close();

        return tasksList;
    }

    private String getOrderBy(SortSettings sortSettings)
    {
        String orderBy = "";

        if(sortSettings != null)
        {
            // set the column to order by
            switch(sortSettings.getSortBy())
            {
                case NAME:
                    orderBy = "name";
                    break;

                case PRIORITY:
                    orderBy = "priority";
                    break;

                case COMPLETED:
                    orderBy = "done";
                    break;

                case CREATION_DATE:
                    orderBy = "creation_date";
                    break;

                case DUE_DATE:
                    // has_due_date DESC, so it will always show tasks with due date first(doesn't matter if asc or desc)
                    orderBy = "has_due_date DESC, due_date";
                    break;
            }

            // set the order
            switch(sortSettings.getSortOrder())
            {
                case ASCENDING:
                    orderBy += " ASC";
                    break;

                case DESCENDING:
                    orderBy += " DESC";
                    break;


            }
        }

        return orderBy;
    }

    /* Attachments */
    public ArrayList<BaseAttachment> getAttachmentsFromTask(int taskId)
    {
        ArrayList<BaseAttachment> attachments = getImagesDataFromTask(taskId);
        attachments.addAll(getVideosFromTask(taskId));

        return attachments;
    }

    /* Image attachments */
    public void addImageToTask(int taskId, ImageAttachment imageAttachment)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("task_id", taskId);
        values.put("data", imageAttachment.getImageAsDataArray());

        db.insertWithOnConflict("task_attachment_image", null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public ImageAttachment getImageDataFromTask(int attachmentId)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("task_attachment_image", null, "id=?", new String[] {String.valueOf(attachmentId)}, null, null, null);
        if(cursor.moveToFirst())
        {
            byte[] data = cursor.getBlob(cursor.getColumnIndex("data"));

            return new ImageAttachment(attachmentId, EAttachmentType.IMAGE, data);
        }

        return null;
    }

    public ArrayList<BaseAttachment> getImagesDataFromTask(int taskId)
    {
        ArrayList<BaseAttachment> imageAttachments = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("task_attachment_image", null, "task_id=?", new String[] {String.valueOf(taskId)}, null, null, null);
        while(cursor.moveToNext())
        {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            byte[] data = cursor.getBlob(cursor.getColumnIndex("data"));

            imageAttachments.add(new ImageAttachment(id, EAttachmentType.IMAGE, data));
        }

        return imageAttachments;
    }

    public void deleteImageFromTask(int imageId)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete("task_attachment_image", "id=?", new String[] {String.valueOf(imageId)});
        db.close();
    }

    public void deleteAllImagesFromTask(int taskId)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete("task_attachment_image", "task_id=?", new String[] {String.valueOf(taskId)});
        db.close();
    }

    /* Video attachments */
    public void addVideoToTask(int taskId, VideoAttachment videoAttachment)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("task_id", taskId);
        values.put("uri", videoAttachment.getVideoFilepath());

        db.insertWithOnConflict("task_attachment_video", null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public VideoAttachment getVideoFromTask(int attachmentId)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("task_attachment_video", null, "id=?", new String[] {String.valueOf(attachmentId)}, null, null, null);
        if(cursor.moveToFirst())
        {
            String uri = cursor.getString(cursor.getColumnIndex("uri"));

            return new VideoAttachment(attachmentId, EAttachmentType.VIDEO, uri);
        }

        return null;
    }

    public ArrayList<BaseAttachment> getVideosFromTask(int taskId)
    {
        ArrayList<BaseAttachment> videoAttachments = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("task_attachment_video", null, "task_id=?", new String[] {String.valueOf(taskId)}, null, null, null);
        while(cursor.moveToNext())
        {
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            String uri = cursor.getString(cursor.getColumnIndex("uri"));

            videoAttachments.add(new VideoAttachment(id, EAttachmentType.VIDEO, uri));
        }

        return videoAttachments;
    }

    public void deleteVideoFromTask(int attachmentId)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete("task_attachment_video", "id=?", new String[] {String.valueOf(attachmentId)});
        db.close();
    }

    public void deleteAllVideosFromTask(int taskId)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete("task_attachment_video", "task_id=?", new String[] {String.valueOf(taskId)});
        db.close();
    }

}
