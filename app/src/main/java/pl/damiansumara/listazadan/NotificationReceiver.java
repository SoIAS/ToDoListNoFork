package pl.damiansumara.listazadan;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;

/**
 * Created by Dawid Wdowiak on 1/18/2018.
 */

public class NotificationReceiver extends BroadcastReceiver
{
    private final static String notificationChannelId = "ToDoChannel_Overview";
    private final static String notificationChannelName = "Overview channel";
    private final static String notificationChannelDescription = "Overview channel, sends notifications about amount of overdue and due in 24 hours tasks.";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        DbHelper dbHelper = new DbHelper(context);

        createNotificationChannel(context, notificationChannelId, notificationChannelName, notificationChannelDescription);

        PushOverdueAmountNotification(dbHelper, context);
        PushDueInADayAmountNotification(dbHelper, context);

        dbHelper.close();
    }

    private void PushOverdueAmountNotification(DbHelper dbHelper, Context context)
    {
        final ArrayList<ToDoTask> tasks = dbHelper.getTaskList(new SortSettings(), EFilterType.OVERDUE);
        if(tasks.size() > 0)
        {
            sendBasicNotification(context, notificationChannelId, 1, "Overdue tasks", "You have " + tasks.size() + " overdue tasks");
        }
    }

    private void PushDueInADayAmountNotification(DbHelper dbHelper, Context context)
    {
        final ArrayList<ToDoTask> tasks = dbHelper.getTasksDueIn(new SortSettings(), 24 * 60 * 60 * 1000);
        if(tasks.size() > 0)
        {
            sendBasicNotification(context, notificationChannelId, 2, "Tasks due in 24 hours", "You have " + tasks.size() + " tasks due in 24 hours");
        }
    }

    protected void createNotificationChannel(Context context, String id, String name, String description)
    {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
        mChannel.setDescription(description);
        mChannel.enableLights(true);

        mChannel.setLightColor(Color.BLUE);
        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        mNotificationManager.createNotificationChannel(mChannel); // todo try catch
    }

    protected void sendBasicNotification(Context context, String channelId, int notificationId, String notificationTitle, String notificationText)
    {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.addtask)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText);

        Intent resultIntent = new Intent(context, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class); // back stack
        stackBuilder.addNextIntent(resultIntent); // intent to start with
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notificationId, mBuilder.build());
    }
}
