package pl.damiansumara.listazadan;

import android.app.Activity;
import android.os.Bundle;

import com.github.chrisbanes.photoview.PhotoView;


/**
 * Created by Dawid Wdowiak on 1/16/2018.
 */

public class ImageAttachmentDetailActivity extends Activity
{
    public static final String ARGUMENT_IMAGE_ATTACHMENT_ID = "IMAGE_ATTACHMENT_ID";


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_attachment_detail);

        final int attachmentId = getIntent().getIntExtra(ARGUMENT_IMAGE_ATTACHMENT_ID, -1);

        DbHelper dbHelper = new DbHelper(getApplicationContext());
        final ImageAttachment imageAttachment = dbHelper.getImageDataFromTask(attachmentId);
        dbHelper.close();

        if(attachmentId < 0 || imageAttachment == null)
        {
            finish();
        }

        PhotoView photoView = findViewById(R.id.attachmentDetail_photoView);
        photoView.setImageBitmap(imageAttachment.getImageAsBitmap());
    }
}