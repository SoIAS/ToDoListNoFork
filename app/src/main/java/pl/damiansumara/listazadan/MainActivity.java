package pl.damiansumara.listazadan;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class MainActivity extends AppCompatActivity
{

    static final String fileName = "TaskList.txt";
    static final int PERMISSION_REQUEST_EXPORT_LIST_TO_FILE = 1;

    DbHelper dbHelper;
    ToDoAdapter mAdapter;
    ListView lstTask;

    SortSettings listSort;
    EFilterType filterType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DbHelper(this);


        lstTask = findViewById(R.id.lstTask);
        lstTask.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                // TODO: HAX for now, getting the id from delte button
                int taskId = (int)view.findViewById(R.id.taskrow_delete).getTag();

                Intent intent = new Intent(getApplicationContext(), TaskDetailActivity.class);
                intent.putExtra(TaskDetailActivity.EXTRA_TASK_ID, taskId);
                startActivity(intent);
            }
        });

        listSort = new SortSettings(ESortBy.NAME, ESortOrder.ASCENDING);
        filterType = EFilterType.NONE;

        addtasks();
        loadTaskList();

        createOverviewNotificationBroadcaster();
        createDetailNotificationBroadcaster();
    }

    private void createOverviewNotificationBroadcaster()
    {
        Intent alarmBroadcaster = new Intent(this, NotificationReceiver.class);
        final boolean isAlarmRunning = (PendingIntent.getBroadcast(this, 0, alarmBroadcaster, PendingIntent.	FLAG_UPDATE_CURRENT) != null);

       //testing if(!isAlarmRunning)
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmBroadcaster, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 6); // Every day around 6am

            // tester
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 1, pendingIntent);

            //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    private void createDetailNotificationBroadcaster()
    {
        Intent alarmBroadcaster = new Intent(this, TaskNotificationReceiver.class);
        final boolean isAlarmRunning = (PendingIntent.getBroadcast(this, 0, alarmBroadcaster, PendingIntent.	FLAG_UPDATE_CURRENT) != null);

       //testing if(!isAlarmRunning)
        {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmBroadcaster, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

            // tester
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 1, pendingIntent);

            final long threeHoursInMilliseconds = 3 * 60 * 60 * 1000;
            //alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), threeHoursInMilliseconds, pendingIntent);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        loadTaskList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_newtask:
                Intent intent = new Intent(getApplicationContext(), AddEditTaskActivity.class);
                startActivity(intent);
                break;

            case R.id.menu_filter:
                showFilterPopupMenu();
                break;

            case R.id.menu_sort:
                showSortingPopupMenu();
                break;

            case R.id.menu_removeCompletedTasks:
            {
                ConfirmationDialog dialog = new ConfirmationDialog(this, new ConfirmationDialog.ICustomDialogCallback() {
                    @Override
                    public void onConfirmation() {
                        dbHelper.deleteCompletedTasks();
                        loadTaskList();
                    }

                    @Override
                    public void onCancel() {

                    }
                });

                dialog.setConfirmationText("Are you sure you want to remove ALL COMPLETED tasks?");
                dialog.show();
                break;
            }

            case R.id.menu_removeAllTasks:
            {
                ConfirmationDialog dialog = new ConfirmationDialog(this, new ConfirmationDialog.ICustomDialogCallback() {
                    @Override
                    public void onConfirmation() {
                        dbHelper.deleteAllTasks();
                        loadTaskList();
                    }

                    @Override
                    public void onCancel() {

                    }
                });

                dialog.setConfirmationText("Are you sure you want to remove ALL tasks?");
                dialog.show();
                break;
            }

            case R.id.menu_saveToFile:
                exportTasksPermissionRequest();
                break;
        }

        return true;
    }

    private void showSortingPopupMenu()
    {
        PopupMenu popup = new PopupMenu(getApplicationContext(), findViewById(R.id.menu_sort));
        popup.getMenuInflater().inflate(R.menu.sort_tasks, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                switch(item.getItemId())
                {
                    case R.id.sortby_name:
                        listSort.changeSortBy(ESortBy.NAME);
                        break;

                    case R.id.sortby_priority:
                        listSort.changeSortBy(ESortBy.PRIORITY);
                        break;

                    case R.id.sortby_completed:
                        listSort.changeSortBy(ESortBy.COMPLETED);
                        break;

                    case R.id.sortby_creationDate:
                        listSort.changeSortBy(ESortBy.CREATION_DATE);
                        break;

                    case R.id.sortby_dueDate:
                        listSort.changeSortBy(ESortBy.DUE_DATE);
                        break;
                }
                loadTaskList();

                return true;
            }
        });

        popup.show();
    }

    private void showFilterPopupMenu()
    {
        PopupMenu popup = new PopupMenu(getApplicationContext(), findViewById(R.id.menu_filter));
        popup.getMenuInflater().inflate(R.menu.filter_tasks, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                switch(item.getItemId())
                {
                    case R.id.filter_none:
                        filterType = EFilterType.NONE;
                        break;

                    case R.id.filter_notCompleted:
                        filterType = EFilterType.NOT_COMPLETED;
                        break;

                    case R.id.filter_completed:
                        filterType = EFilterType.COMPLETED;
                        break;

                    case R.id.filter_overdue:
                        filterType = EFilterType.OVERDUE;
                        break;

                    case R.id.filter_dueToday:
                        filterType = EFilterType.DUE_TODAY;
                        break;

                    case R.id.filter_dueIn24h:
                        filterType = EFilterType.DUE_IN_24H;
                        break;

                    case R.id.filter_dueIn7d:
                        filterType = EFilterType.DUE_IN_7D;
                        break;

                    case R.id.filter_dueIn30d:
                        filterType = EFilterType.DUE_IN_30D;
                        break;

                }
                loadTaskList();

                return true;
            }
        });

        popup.show();
    }

    // temp for task adding
    private void addtasks()
    {
        Random random = new Random();
        for(int i = 0; i < 5; ++i)
        {
            boolean doneTask = random.nextInt(2) == 0;
            dbHelper.newTask(new ToDoTask(0, "nazwa", random.nextInt(getResources().getStringArray(R.array.priority_strings).length), doneTask, new Date(System.currentTimeMillis()), new Date(0), new Date(0)));
        }
    }

    private void loadTaskList()
    {
        ArrayList<ToDoTask> taskList = dbHelper.getTaskList(listSort, filterType);

        if(mAdapter==null) {
            mAdapter = new ToDoAdapter(taskList,getApplicationContext());
            lstTask.setAdapter(mAdapter);
        }
        else{
            mAdapter.clear();
            mAdapter.addAll(taskList);
            mAdapter.notifyDataSetChanged();
        }
    }

    public void completeCheckboxClicked(View view)
    {
        int taskId = (int)view.getTag();
        dbHelper.setTaskStatus(taskId, ((CheckBox)view).isChecked());

        loadTaskList();
    }

    public void deleteTask(final View view)
    {
        ConfirmationDialog dialog = new ConfirmationDialog(this, new ConfirmationDialog.ICustomDialogCallback() {
            @Override
            public void onConfirmation()
            {
                final int taskId = (int)view.getTag();
                dbHelper.deleteTask(taskId);
                loadTaskList();
            }

            @Override
            public void onCancel()
            {

            }
        });

        dialog.setConfirmationText("Are you sure you want to remove this task?");
        dialog.show();
    }

    private void exportTasksPermissionRequest()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_EXPORT_LIST_TO_FILE);
        }
        else
        {
            // already got permission
            exportTasksToFile(fileName);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int grantResults[])
    {
        switch (requestCode)
        {
            case PERMISSION_REQUEST_EXPORT_LIST_TO_FILE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    exportTasksToFile(fileName);
                }
                else
                {
                    Toast.makeText(this, "Cannot export task list without permission", Toast.LENGTH_LONG).show();
                }
        }
    }

    private void exportTasksToFile(String fileName)
    {
        File targetDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS); // write to an external storage
        File taskFile = new File(targetDir, fileName);
        if (!taskFile.exists())
        {
            targetDir.mkdirs();
            try
            {
                taskFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        try
        {
            ArrayList<ToDoTask> taskList = dbHelper.getTaskList(listSort, filterType);

            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(taskFile, false));
            for (ToDoTask task : taskList)
            {
                bufferedWriter.append(task.toString(getApplicationContext()) + "\n\n");
            }

            bufferedWriter.flush();
            bufferedWriter.close();

            Toast.makeText(getApplicationContext(), R.string.export_text, Toast.LENGTH_LONG).show();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

}
