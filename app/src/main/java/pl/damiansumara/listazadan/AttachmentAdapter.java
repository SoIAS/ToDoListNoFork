package pl.damiansumara.listazadan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Dawid Wdowiak on 1/15/2018.
 */

public class AttachmentAdapter extends ArrayAdapter
{
    private ArrayList<BaseAttachment> data;
    Context context;


    // view lookup cache
    private static class ViewHolder
    {
        ImageView imageView;
        ImageView videoPlayButton;
        ImageButton deleteButton;
    }

    public AttachmentAdapter(ArrayList<BaseAttachment> data, Context context)
    {
        super(context, R.layout.row, data);
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }

    @Override
    public BaseAttachment getItem(int position)
    {
        return data.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        View result;


        if(convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachment_item, parent, false);

            viewHolder.imageView = convertView.findViewById(R.id.attachment_imageView);
            viewHolder.videoPlayButton = convertView.findViewById(R.id.attachment_videoPlayIcon);
            viewHolder.deleteButton = convertView.findViewById(R.id.attachment_deleteButton);

            convertView.setTag(viewHolder);
            result = convertView;
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        final BaseAttachment attachment = getItem(position);
        if(attachment.getAttachmentType() == EAttachmentType.IMAGE)
        {
            final ImageAttachment imageAttachment = (ImageAttachment)attachment;
            viewHolder.imageView.setImageBitmap(imageAttachment.getImageAsBitmap());
            viewHolder.videoPlayButton.setVisibility(View.INVISIBLE);
        }
        else if(attachment.getAttachmentType() == EAttachmentType.VIDEO)
        {
            final VideoAttachment videoAttachment = (VideoAttachment) attachment;
            viewHolder.imageView.setImageBitmap(videoAttachment.getThumbnail());
            viewHolder.videoPlayButton.setVisibility(View.VISIBLE);
        }

        viewHolder.deleteButton.setTag(position); // Position is the tag, so we can check if it is video or image attachment
        return result;
    }
}
