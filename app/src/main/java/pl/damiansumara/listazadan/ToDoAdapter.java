package pl.damiansumara.listazadan;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import android.text.format.DateFormat;
import java.util.ArrayList;

/**
 * Created by Dawid Wdowiak on 1/11/2018.
 */

public class ToDoAdapter extends ArrayAdapter
{
    private ArrayList<ToDoTask> data;
    Context context;


    // view lookup cache
    private static class ViewHolder
    {
        CheckBox completeCheckBox;
        ImageView priorityImage;
        TextView nameTextView;
        TextView dueDateYearTextView;
        TextView dueDateDayMonthTextView;
        ImageButton deleteButton;
    }

    public ToDoAdapter(ArrayList<ToDoTask> data, Context context)
    {
        super(context, R.layout.row, data);
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }

    @Override
    public ToDoTask getItem(int position)
    {
        return data.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        View result;

        if(convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
            viewHolder.completeCheckBox = convertView.findViewById(R.id.taskrow_completeCheckbox);
            viewHolder.priorityImage = convertView.findViewById(R.id.taskrow_priorityImage);
            viewHolder.nameTextView = convertView.findViewById(R.id.taskrow_title);
            viewHolder.dueDateYearTextView = convertView.findViewById(R.id.taskrow_dueDateYear);
            viewHolder.dueDateDayMonthTextView = convertView.findViewById(R.id.taskrow_dueDateDayMonth);
            viewHolder.deleteButton = convertView.findViewById(R.id.taskrow_delete);

            result = convertView;
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        ToDoTask task = getItem(position);
        viewHolder.completeCheckBox.setChecked(task.getDone());
        viewHolder.completeCheckBox.jumpDrawablesToCurrentState(); // remove the animation
        viewHolder.completeCheckBox.setTag(task.getId()); // id as tag for now
        viewHolder.priorityImage.setImageResource(getContext().getResources().obtainTypedArray(R.array.priority_images).getResourceId(task.getPriority(), 0));
        viewHolder.nameTextView.setText(task.getName());

        final boolean showDueDate = task.hasDueDate() && !task.getDone();
        viewHolder.dueDateYearTextView.setVisibility(showDueDate ? TextView.VISIBLE : TextView.GONE);
        viewHolder.dueDateDayMonthTextView.setVisibility(showDueDate ? TextView.VISIBLE : TextView.GONE);
        if(showDueDate)
        {
            final String year = (String)DateFormat.format("yyyy", task.getDueDate());
            final String month =  (String)DateFormat.format("MMM", task.getDueDate());
            final String day =  (String)DateFormat.format("dd", task.getDueDate());

            final boolean isOverdue = task.getDueDate().getTime() < System.currentTimeMillis();


            viewHolder.dueDateYearTextView.setText(year);
            viewHolder.dueDateDayMonthTextView.setText(day + " " + month);

            // TODO: Or just use exclamation icon that indicates overdue?
            viewHolder.dueDateYearTextView.setTextColor(isOverdue ? Color.RED : Color.BLACK);
            viewHolder.dueDateDayMonthTextView.setTextColor(isOverdue ? Color.RED : Color.BLACK);

        }

        viewHolder.deleteButton.setTag(task.getId()); // set the tag to item id
        return result;
    }
}
