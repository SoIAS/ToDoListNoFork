package pl.damiansumara.listazadan;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;

/**
 * Created by Dawid Wdowiak on 1/19/2018.
 */

public class TaskNotificationReceiver extends NotificationReceiver
{
    private final static String notificationChannelId = "ToDoChannel_Task";
    private final static String notificationChannelName = "Tasks channel";
    private final static String notificationChannelDescription = "Tasks channel, sends notification about specific tasks";


    @Override
    public void onReceive(Context context, Intent intent)
    {
        final long dayInMs = (long)24 * 60 * 60 * 1000;

        DbHelper dbHelper = new DbHelper(context);
        ArrayList<ToDoTask> tasksDueIn24 = dbHelper.getTasksDueInForNotifcation(new SortSettings(ESortBy.DUE_DATE, ESortOrder.ASCENDING), dayInMs);

        if(tasksDueIn24.size() > 0)
        {
            createNotificationChannel(context, notificationChannelId, notificationChannelName, notificationChannelDescription);

            for(ToDoTask task : tasksDueIn24)
            {
                sendTaskNotification(context, notificationChannelId, task);
                dbHelper.setNotificationStatus(task.getId(), true);
            }
        }

        dbHelper.close();
    }

    protected void sendTaskNotification(Context context, String channelId, ToDoTask task)
    {
        final long timeDiff = task.getDueDate().getTime() - System.currentTimeMillis();
        if(timeDiff <= 0) return;

        final long hourInMilliseconds = 60 * 60 * 1000;
        final long dayInMilliseconds = 24 * hourInMilliseconds;

        final long dueInDays = timeDiff / dayInMilliseconds;
        final long dueInHours = (timeDiff % dayInMilliseconds) / hourInMilliseconds;

        final String dueInDaysString = dueInDays > 0 ? (dueInDays == 1 ? "1 day" : dueInDays + " days") : "";
        final String dueInHoursString = dueInHours > 0 ? (dueInHours == 1 ? "1 hour" : dueInHours + " hours") : "";
        final String dueInString = dueInDays > 0 && dueInHours > 0 ?
                dueInDaysString + " and " + dueInHoursString :
                dueInDaysString + dueInHoursString;

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.addtask)
                .setContentTitle("Task is due in " + dueInString)
                .setContentText("Task '" + task.getName() +"' is due on " + Helpers.dateToString(task.getDueDate()));

        Intent resultIntent = new Intent(context, TaskDetailActivity.class);
        resultIntent.putExtra(TaskDetailActivity.EXTRA_TASK_ID, task.getId());


        Intent mainActivityIntent = new Intent(context, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(mainActivityIntent);
        stackBuilder.addNextIntent(resultIntent);// intent to start with
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(task.getId(), mBuilder.build());
    }
}
