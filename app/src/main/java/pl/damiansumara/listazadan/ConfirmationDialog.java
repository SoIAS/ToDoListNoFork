package pl.damiansumara.listazadan;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

/**
 * Created by damian on 16.01.18.
 */

public class ConfirmationDialog extends Dialog
        implements android.view.View.OnClickListener
{
    public interface ICustomDialogCallback
    {
        void onConfirmation();
        void onCancel();
    }

    ICustomDialogCallback callback;
    String customConfirmationText;

    public ConfirmationDialog(Activity activity, @NonNull ICustomDialogCallback callback)
    {
        super(activity);

        if(callback == null)
        {
            throw new NullPointerException("Callback is null");
        }

        this.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.confirmation_dialog);

        findViewById(R.id.confirmationDialog_confirm).setOnClickListener(this);
        findViewById(R.id.confirmationDialog_cancel).setOnClickListener(this);

        if(customConfirmationText != null)
        {
            ((TextView) findViewById(R.id.confirmationDialog_text)).setText(customConfirmationText);
        }

    }

    public void setConfirmationText(String text)
    {
        // Do it with a variable, since we cannot change it before onCreate
        customConfirmationText = text;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.confirmationDialog_confirm:
                /*
                // Tak tez mozna, tylko klasa ktora wywoluje customdialog musi implmentowac ICustomDialogCallback
                // W wypadku kasowania tasku, musimy takze przekazac id do dialogu lub gdzies  go zapisac
                if(getContext() instanceof ICustomDialogCallback)
                {
                    ((ICustomDialogCallback) getContext()).onConfirmation();
                }
                */
                callback.onConfirmation();
                break;

            case R.id.confirmationDialog_cancel:
                callback.onCancel();
                break;
        }

        dismiss();
    }
}